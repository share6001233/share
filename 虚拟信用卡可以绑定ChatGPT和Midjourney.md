# 虚拟货币（火币-安全），国人玩的最多的平台，中文好入手

## Dupay（不需要梯子）

### 注册-微信

1、建议大家用微信扫描下面二维码进行注册

<img src="https://gitlab.com/share6001233/vpn/-/raw/main/dupay/001.jpg?ref_type=heads" style="zoom: 50%;" />



扫描完后会出现以下界面

<img src="https://gitlab.com/share6001233/vpn/-/raw/main/dupay/002.jpg?ref_type=heads" style="zoom: 25%;" />



### 注册-网页

同样也是，点击 注册 [Dupay注册](https://dupay.one/web-app/register-h5?invitCode=267684&lang=zh-cn)  可以在网站上进行注册，流程一样的

建议使用邮箱去注册，避免暴漏你的个人信息。

### 充值

<img src="https://gitlab.com/share6001233/vpn/-/raw/main/dupay/003.jpg?ref_type=heads" style="zoom:25%;" />

目前Dupay只支持USDT充值，这种充值方式是使用区块链的方式充值的。

【选择主网】时一定要记得自己选的是哪个，好比要都哪条高速公路，走错了就回不了家了。一般选择【TRC20】效率高，支持多个交易所，也是最快的。

<img src="https://gitlab.com/share6001233/vpn/-/raw/main/dupay/004.jpg?ref_type=heads" style="zoom:25%;" />





 <img src="https://gitlab.com/share6001233/vpn/-/raw/main/dupay/005.jpg?ref_type=heads" style="zoom: 33%;" />    

所以需要在虚拟货币交易所进行。这个是绝对安全的，因为是去中心化的，所以监管不到。可以在 [主页](https://gitlab.com/share6001233/share) 中寻找虚拟货币的交易所进行充币，然后再提到这个虚拟信用卡上进行消费。

### 消费

虚拟行用卡有卡号、姓名和CVV安全吗。和国内的信用卡使用是一样的。绑定支付宝微信也可以去消费，因为信用卡里存的是美刀，在国内消费的时候扣除的是RMB，这个比例差不都是1：7。可以看汇率看查账的。

### 提现

同样提现也是先提到区块链的交易所上，然后再返回到支付宝或者微信中。可以在 [主页](https://gitlab.com/share6001233/share) 中寻找虚拟货币的交易所进行提现。

